// let params = new URLSearchParams(window.location.search)
// console.log(params.has('courseId'))
// console.log(params.get('courseId'))
// let courseId = params.get('courseId')
// console.log(courseId)

// let token = localStorage.getItem('token')
// console.log(token)

// 	fetch('http://localhost:8000/api/users/enroll',{
// 		method: 'DELETE',
// 		headers:{
// 			'Content-Type' : 'application/json',
// 			'Authorization' : `Bearer ${token}`
// 		},
// 		body: JSON.stringify({
// 			courseId:courseId
// 		})
// 	})
// 	.then(res => res.json())
// 	.then(data => {
// 		console.log(data)

// 					//redirect the user to the courses page after enrolling

// 					if(data === true){
// 						alert ('Course is Delete')
// 						window.location.replace('./courses.html')

// 					}else {
// 						alert('Something Went Wrong')
// 					}
// 				})
// })

let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");

//how do we get the token from the localStorage?
let token = localStorage.getItem("token");

//this fetch will be run automatically once you get to the page deleteCourse page.
fetch(`http://localhost:8000/api/courses/archive/${courseId}`, {
  method: "PUT",
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((res) => res.json())
  .then((data) => {
    if (data) {
      alert("Course Archived.");
    } else {
      alert("Something Went Wrong.");
    }
  });
