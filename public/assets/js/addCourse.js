let formSubmit = document.querySelector("#createCourse")
// console.log(formSubmit)

//add an event listerner:

formSubmit.addEventListener("submit", (e)=>{	

// Q: what does preventDefacult( do) ?
	// A: it prevent the normal behavior

	e.preventDefault()

	// get value input
	let courseName = document.querySelector('#courseName').value
	let description = document.querySelector('#courseDescription').value
	let price = document.querySelector('#coursePrice').value


	console.log(courseName)
	console.log(courseDescription)
	console.log(coursePrice)

	let token = localStorage.getItem('token')

	console.log(token)

	// create a fect request to add a new course:

	fetch('http://localhost:8000/api/courses', {
		method: 'POST',
		headers: {

			"Content-Type": "application/json",
			"Authorization": `Bearer ${token}` 
			//We are sending a token through HTTP. That is to say that we are "giving authorization to the bearer of this token."

		},
		body: JSON.stringify({

			name: courseName,
			description: description,
			price: price,

		})
	})
	.then(res => res.json())
	.then(data => {

		if(data){

			window.location.replace("./courses.html")

		} else {

			alert("Something went wrong. Course not added.")
		}

	})


})