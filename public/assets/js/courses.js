let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin") === "true";
let addButton = document.querySelector("#adminButton");
let cardFooter;

if (adminUser === false || adminUser === null) {
  addButton.innerHTML = null;
} else {
  addButton.innerHTML = `

	<div class="col-md-2 offset-md-10">
	<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
	</div>


	`;
}

if (adminUser === true) {
  console.log("Admin");
  fetch("http://localhost:8000/api/courses/", {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
    .then((res) => res.json())
    .then((data) => {
      let courseData;
      if (data.length < 1) {
        courseData = "no courses available";
      } else {
        courseData = data
          .map((course) => {
            if (course.isActive === true) {
              cardFooter = `
							<a href="./editCourse.html?courseId=${course._id}" 
							value={course._id} 
							class = 'btn btn-warning text-white btn-block editButton'
							> Edit</a>
							<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Course</a>


					<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">Archive Course</a>


				
					`;
            } else {
              cardFooter = `
							<a href="./editCourse.html?courseId=${course._id}" 
							value={course._id} 
							class = 'btn btn-warning text-white btn-block editButton'
							> Edit</a>
							<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Course</a>
	
						<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block">Activate Course</a>
	
	
					
						`;
            }

            return `


					<div class="col-md-6 my-3">
					<div class="card">
					<div class="card-body">
					<h5 class="card-title">${course.name}</h5>

					<p class="card-text text-left">
					${course.description}
					</p>
					<p class="card-text text-rigt">
					₱ ${course.price}
					</p>
					</div>
					<div class="card-footer">
					${cardFooter}
					</div>

					</div>

					</div>
					`;
          })

          .join("");
      }

      let container = document.querySelector("#coursesContainer");

      container.innerHTML = courseData;
    });
} else {
  console.log("notadmin");
  fetch("http://localhost:8000/api/courses/")
    .then((res) => res.json())
    .then((data) => {
      let courseData;

      //to check if there are any active courses
      if (data.length < 1) {
        courseData = "No Courses Available";
      } else {
        courseData = data
          .map((course) => {
            //each item in the array is iterated

            //conditionally render the content of our cardFooter with buttons that only an admin can see or a button that only a regular user can see.
            //If statement runs IF the current user is a regular user or a guest
            if (adminUser == "false" || !adminUser) {
              //button to go to specific course
              //What do we call an added value in the url? URL Parameter
              cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Course</a>`;
            } else {
              //if current user is an admin

              //button to archive/deactivate
              cardFooter = `
					<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Course</a>
			
					<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Activate Course</a>
					`;
            }

            //will be returned as each item in the new array
            return `
					<div class="col-md-6 my-3">
					<div class="card">
					<div class="card-body">
					<h5 class="card-title">${course.name}</h5>
					<p class="card-text text-left">${course.description}</p>
					<p class="card-text text-right">${course.price}</p>
					</div>
					<div class="card-footer">
					${cardFooter}
					</div>
					</div>
					</div>

					`;
          })
          .join("");

        //join() - joins all of the array elements/items into a string. The argument passed in the method becomes the separator for each item because by default, each Item is separated by a comma.
      }

      //store the element with the id coursesContainer in a variable
      let container = document.querySelector("#coursesContainer");

      container.innerHTML = courseData;
    });
}
