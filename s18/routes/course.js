const router = require("express").Router();
const Course = require("./../models/course");
const CourseController = require("./../controllers/course");
const auth = require("./../auth");
//api/courses
// get all courses
router.get("/", (req, res) => {
  const decodedToken = auth.decode(req.headers.authorization);

  CourseController.getAll(decodedToken).then((courses) => res.send(courses));
});

// get course by id
router.get("/:id", (req, res) => {
  CourseController.get(req.params.id).then((result) => res.send(result));
});

// add course
router.post("/", auth.verify, (req, res) => {
  CourseController.add(req.body).then((result) => res.send(result));
});

// update course
router.put("/", auth.verify, (req, res) => {
  CourseController.update(req.body).then((result) => res.send(result));
});

// delete course
//api/courses/ put anything
router.delete("/:id", auth.verify, (req, res) => {
  CourseController.archive(req.params.id).then((result) => res.send(result));
});

router.put("/archive/:id", auth.verify, (req, res) => {
  console.log("test");
  CourseController.archive(req.params.id).then((result) => res.send(result));
});

router.put("/activate/:id", auth.verify, (req, res) => {
  console.log("test");
  CourseController.activate(req.params.id).then((result) => res.send(result));
});

module.exports = router;
