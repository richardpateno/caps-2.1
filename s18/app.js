const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');



const port = process.env.PORT || 8000;
const app = express();
require('dotenv').config();

// database connection
mongoose.connect(process.env.DB, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// check connection
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log(`Connected to database`);
});

// const corsOption = {
// 	origin: ['http://localhost:8000'],
// 	optionSuccessStatus:200
// }

// allow reource sharing from all origins
app.use(cors());

// middlewares
// app.use(cors(corsOption));
app.use(express.json());

// routes
const courseRoutes = require('./routes/course');
app.use('/api/courses', courseRoutes);
const userRoutes = require('./routes/user');
app.use('/api/users', userRoutes)

app.listen(port , () => {
	console.log(`App is listening on port ${port}`);
});
